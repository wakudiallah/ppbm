<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CIF;
use DB;
use Session;
class SearchingController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
 
     public function index()
    {
        return view('admin.searching');
    }
     public function searching(Request $request)
    {
         $ic = $request->input('search');
         $ic_s = substr($ic[0],3);

        $cif = CIF::where('ic',$ic)->count();
        if($cif=='0'){
              
               return redirect('/administrator/cif/'.$ic)->with('message', 'success!!');
        }
        else{
            
                        \Session::flash('flash_message','IC telah terdaftar');
       return redirect('/')->with('message', 'success!!');
              
        }

        	
    }

     public function cif($ic)
    {
         
         $ics = substr($ic,0,4);
          $i = substr($ic,0,1);
           $i2 = substr($ic,0,4);
        $i_x = substr($ic,2,2);

           // $queries = DB::table('n24_0010')
             //   ->where('IC', 'LIKE', '%'.$ic.'%')
               // ->first();
         $s='0010';
         $e='1708';
         if($i=='T'){
             $queries = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();

            $count = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->count();
         }
         else if($i=='R'){
             $queries = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();

            $count = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->count();
         }
         else if($i=='M'){
             $queries = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();

            $count = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->count();
         }
         else if($i=='I'){
             $queries = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();

            $count = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->count();
         }
         else if($i=='G'){
             $queries = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();

            $count = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->count();
         }
         else if($i=='J'){
             $queries = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();

            $count = DB::table('n24_'.$i)
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->count();
         }

       else if(($i2>=0001) || ($i2<=9912))
       {
              if($i_x>='13'){
               
                return view('admin.searching.step1',compact('ic','ics','queries','i','religion','residence','i_x'))->withErrors(['IC tidak ditemukan dalam database']);
            }
            else
            {
                $queries = DB::table('n24_'.$ics)
                 ->where('IC', 'LIKE', '%'.$ic.'%')
                ->first();

                $count = DB::table('n24_'.$ics)
                ->where('IC', 'LIKE', '%'.$ic.'%')
                ->count();
            }
        }
         else if(($i!='J') || ($i!='M') || ($i!='G') || ($i!='I') || ($i!='R') || ($i!='T')){
                return view('admin.searching.step1',compact('ic','ics','queries','i','religion','residence','i_x'))->withErrors(['IC tidak ditemukan dalam database']);
         }
        else{
                return view('admin.searching.step1',compact('ic','ics','queries','i','religion','residence','i_x'))->withErrors(['IC tidak ditemukan dalam database']);
        }
            
         



        
          
        
       /* if($ics == '0010'){
            $queries = DB::table('n24_0010')
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();
        }
        else if($ics == '0006'){
            $queries = DB::table('n24_0006')
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();
        }
        else if($ics == '0004'){
            $queries = DB::table('n24_0004')
            ->where('IC', 'LIKE', '%'.$ic.'%')
            ->first();
        }*/
        if($count=='1'){
            return view('admin.searching.step1',compact('ic','ics','queries','i','religion','residence','i_x'));
        }
        else{
            return view('admin.searching.step1_new',compact('ic','ics','queries','i','religion','residence','i_x'))->withErrors(['IC tidak ditemukan dalam database']);
        }
        
    }
}
