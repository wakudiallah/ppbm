            <div class="row">
                <div class="col-lg-12">
                    @if ($message = Session::get('delete')) 
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ $message }}
                    </div>

                    @elseif($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ $message }}
                    </div>

                    @elseif($message = Session::get('update'))
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ $message }}
                    </div>
                    @endif
                </div>
            </div>