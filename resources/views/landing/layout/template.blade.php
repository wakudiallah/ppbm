<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Survey</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="{{asset('landing/images/favicon.ico')}}"/>
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('landing/css/bootstrap.min.css')}}" rel="stylesheet">
    
    <!-- Slick slider -->
    <link href="{{asset('landing/css/slick.css')}}" rel="stylesheet">
    <!-- Theme color -->
    <link id="switcher" href="{{asset('landing/css/theme-color/default-theme.css')}}" rel="stylesheet">

    <!-- Main Style -->

    <link href="{{asset('landing/css/style.css')}}" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800" rel="stylesheet">
	<!-- Montserrat for title -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
 
 
  </head>
  <body>

  	<style type="text/css">
		.countdown {
		  text-align: center;
		  margin-bottom: 80px;
		  margin-top: 40px;
		}
		.countdown .timeel {
		  display: inline-block;
		  padding: 30px;
		  background: #151515;
		  background: rgba(222, 26, 16, 0.5); 
		  margin: 0;
		  color: white;
		  font-size: 20px;
		  min-width: 2.6rem;
		  margin-left: 13px;
		  border: 2px solid #fff;
		  border-radius: 10px 0 0 10px;

		}
		.countdown span[class*="timeRef"] {
		  border-radius: 0 10px 10px 0;
		  margin-left: 0;
		  color: #fefcfc;
		  background: rgba(222, 26, 16, 0.5); 
		  
		}
		.top-0{

		  margin-top: 0px !important;
		}
  	</style>
  	
  	@include('landing/layout/header')
	
	<!-- Start main content -->
	<main role="main">
		<!-- Start About -->
		<section id="mu-about">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-about-area">
							<!-- Start Feature Content -->
							<div class="row">
								<div class="col-md-6">
									<div class="mu-about-left">
										<img class="" src="{{asset('landing/images/about.jpg')}}" alt="Men Speaker">
									</div>
								</div>
								<div class="col-md-6">
									<div class="mu-about-right">
										<h2>About The Conference</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam aliquam distinctio magni enim error commodi suscipit nobis alias nulla, itaque ex, vitae repellat amet neque est voluptatem iure maxime eius!</p>

										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus in accusamus qui sequi nisi, sint magni, ipsam, porro nesciunt id veritatis quaerat ipsum consequatur laborum, provident veniam quibusdam placeat quam?</p>

										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate perspiciatis magni omnis excepturi, cumque reiciendis.</p>
									</div>
								</div>
							</div>
							<!-- End Feature Content -->

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End About -->

		

		<!-- Start Venue -->
		<section id="mu-venue">
			<div class="mu-venue-area">
				<div class="row">

					<div class="col-md-6">
						<div class="mu-venue-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3508.8176744277202!2d-81.47150788457147!3d28.424757900613237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88e77e378ec5a9a9%3A0x2feec9271ed22c5b!2sOrange+County+Convention+Center!5e0!3m2!1sen!2sbd!4v1503833952781" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>

					<div class="col-md-6">
						<div class="mu-venue-address">
							<h2>VENUE <i class="fa fa-chevron-right" aria-hidden="true"></i></h2>
							<h3>Orange County Convention Center</h3>
							<h4>9800 International Dr, Orlando, FL 32819, USA</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem reiciendis incidunt accusantium porro amet repellendus hic corporis fugiat officiis, sequi iste distinctio possimus dignissimos, veniam quae delectus. Fuga, modi, perferendis!</p>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- End Venue -->

		
		<!-- Start Contact -->
		<section id="mu-contact">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-contact-area">

							<div class="mu-title-area">
								<h2 class="mu-heading-title">Contact Us</h2>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
							</div>

							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Contact -->

	</main>
	
	<!-- End main content -->	
			
			
	<!-- Start footer -->
	<footer id="mu-footer" role="contentinfo">
			<div class="container">
				<div class="mu-footer-area">
					<div class="mu-footer-top">
						<div class="mu-social-media">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-google-plus"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
							<a href="#"><i class="fa fa-youtube"></i></a>
						</div>
					</div>
					<div class="mu-footer-bottom">
						<p class="mu-copy-right">&copy; Copyright <a rel="nofollow" href="http://markups.io">markups.io</a>. All right reserved.</p>
					</div>
				</div>
			</div>

	</footer>
	<!-- End footer -->

	
	
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap -->
    <script src="{{asset('landing/js/bootstrap.min.js')}}"></script>
	<!-- Slick slider -->
    <script type="text/javascript" src="{{asset('landing/js/slick.min.js')}}"></script>
    <!-- Event Counter -->
    <script type="text/javascript" src="{{asset('landing/js/jquery.countdown.min.js')}}"></script>
    <!-- Ajax contact form  -->
    <script type="text/javascript" src="{{asset('landing/js/app.js')}}"></script>
  
       
	
    <!-- Custom js -->
	<script type="text/javascript" src="{{asset('landing/js/custom.js')}}"></script>


	<script type="text/javascript">
		/*
		 * Basic Count Down to Date and Time
		 * Author: @guwii / guwii.com
		 * https://guwii.com/bytes/easy-countdown-to-date-with-javascript-jquery/
		*/
		window.onload = function() {
		 
		  countDownToTime("Apr 28, 2019 00:00:00", 'countdown1'); // ****** Change this line!
		}
		function countDownToTime(countTo, id) {
		  countTo = new Date(countTo).getTime();
		  var now = new Date(),
		      countTo = new Date(countTo),
		      timeDifference = (countTo - now);
		      
		  var secondsInADay = 60 * 60 * 1000 * 24,
		      secondsInAHour = 60 * 60 * 1000;

		  days = Math.floor(timeDifference / (secondsInADay) * 1);
		  hours = Math.floor((timeDifference % (secondsInADay)) / (secondsInAHour) * 1);
		  mins = Math.floor(((timeDifference % (secondsInADay)) % (secondsInAHour)) / (60 * 1000) * 1);
		  secs = Math.floor((((timeDifference % (secondsInADay)) % (secondsInAHour)) % (60 * 1000)) / 1000 * 1);

		  var idEl = document.getElementById(id);
		  idEl.getElementsByClassName('days')[0].innerHTML = days;
		  idEl.getElementsByClassName('hours')[0].innerHTML = hours;
		  idEl.getElementsByClassName('minutes')[0].innerHTML = mins;
		  idEl.getElementsByClassName('seconds')[0].innerHTML = secs;

		  clearTimeout(countDownToTime.interval);
		  countDownToTime.interval = setTimeout(function(){ countDownToTime(countTo, id); },1000)
		  again = false;
		}

	</script>

	
	
    
  </body>
</html>