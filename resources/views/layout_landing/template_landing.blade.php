<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Add Your favicon here -->
    <!--<link rel="icon" href="img/favicon.ico">-->

    <title>Partai Pribumi Bersatu Malaysia</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('layout/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="{{asset('layout/css/animate.min.css')}}" rel="stylesheet">

    <link href="{{asset('layout/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="{{asset('layout/css/style.css')}}" rel="stylesheet">

    <link rel="icon" href="{{asset('layout/img/fav/favicon.ico')}}" type="image/x-icon">
    
</head>

@include('layout_landing/header')

@yield('content_landing')


<script src="{{asset('layout/js/jquery-2.1.1.js')}}"></script>
<script src="{{asset('layout/js/pace.min.js')}}"></script>
<script src="{{asset('layout/js/bootstrap.min.js')}}"></script>
<script src="{{asset('layout/js/classie.js')}}"></script>
<script src="{{asset('layout/js/cbpAnimatedHeader.js')}}"></script>
<script src="{{asset('layout/js/wow.min.js')}}"></script>
<script src="{{asset('layout/js/inspinia.js')}}"></script>
</body>
</html>