<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/coba', function () {
    return view('landing/layout/template');
});


Route::get('/', function () {
    return view('index');
});

Route::get('/tes', function () {
    return view('tes');
});

Route::get('/administrator', function () {
    return view('admin/dashboard');
});

Route::get('/administrator/searching', function () {
    return view('admin/searching');
});
Route::get('/administrator/cif/{ic}', 'Admin\SearchingController@cif');
Route::post('/searching_ic', 'Admin\SearchingController@searching');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
